package lesson2Site.commonElements;

public class Link {

    private String title;
    private String url;
    private String color;
    private boolean touched;

    public Link() {

    }

    public Link(String title) {
        this.title = title;
    }

    public Link(String title, String url, String color, boolean touched) {
        this.title = title;
        this.url = url;
        this.color = color;
        this.touched = touched;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public void click() {
        System.out.println("Link '" + title + "' was clicked");
    }
}
