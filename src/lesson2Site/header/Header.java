package lesson2Site.header;

import lesson2Site.commonElements.ImageLink;
import lesson2Site.commonElements.Link;
import lesson2Site.header.appsMenu.AppsMenu;

public class Header {

    public Link gmail;
    public Link images;
    public AppsMenu appsMenu;

    public Header() {
        gmail =  new Link("Gmail");
        images = new Link("Images");
        appsMenu = new AppsMenu();
    }
}
